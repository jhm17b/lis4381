# LIS4381

## Jack H. Miller

### Assignment 4 Requirements:

*4 Parts:*

1. Screenshot of Petstore clientside validation working
2. Screenshot of Petstore clientside validation not working
3. Screenshot of running My Online Portfolio homepage
4. Screenshots of Java Skillsets 10-12
  

#### Assignment Screenshots:

First Screen            |  Second Screen
:-------------------------:|:-------------------------:
![Application's First User Interface screen](img/Clientsidevalidation.JPG)  |  ![Application's Second User Interface screen](img/Clientsidevalidationpt2.JPG)

Correct Validation
![Application's First User Interface screen](img/Clientsidevalidation.JPG)

Incorrect Validation
![Application's Second User Interface screen](img/Clientsidevalidationpt2.JPG)

Homepage slide 1            |  Homepage slide 2  |  Homepage slide 3
:-------------------------:|:-------------------------:|:-------------------------:
![Application's First User Interface screen](img/Homepage1.JPG)  |  ![Application's Second User Interface screen](img/Homepage2.JPG)   |   ![Application's Second User Interface screen](img/Homepage3.JPG)

Slide 1
![Application's First User Interface screen](img/Homepage1.JPG)

Slide 2
![Application's First User Interface screen](img/Homepage2.JPG)

Slide 3
![Application's First User Interface screen](img/Homepage3.JPG)


Java SS10 ArrayLists           |  Java SS11 Alpha Numeric Special Test  |  Java SS12 Temperature Conversion
:-------------------------:|:-------------------------:|:-------------------------:
![Java SS10 Array Lists ](img/JavaSS10.JPG)  |  ![Java SS11 Alpha Numeric Special Test ](img/JavaSS11.JPG)  |  ![Java SS12 Temperature Conversion ](img/JavaSS12.JPG)

Java SS10 Array Lists
![Java SS10 Array Lists ](img/JavaSS10.JPG)

Java SS11 Alpha Numeric Special Test
![Java SS11 Alpha Numeric Special Test](img/JavaSS11.JPG)

Java SS12 Temperature Conversion
![Java SS12 Temperature Conversion ](img/JavaSS12.JPG)


