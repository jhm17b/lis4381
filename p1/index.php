<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Jack H. Miller Senior IT Major">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Project1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Project 1 Requirements:</strong> 5 Parts:

Screenshot of running application's first user interface
Screenshot of running application's second user interface
Java SkillSet 7 Random Array Data
Java SkillSet 8 Largest of Three Numbers
Java SkillSet 9 Array Runtime Data Validation
				</p>

				<h4>My Business Card App</h4>
				<img src="imgs/screen1.png" class="img-responsive center-block" alt="My Business Card Screenshot pt2" width="20%" height="20%">

				<h4>My Business Card App Pt.2</h4>
				<img src="imgs/screen2.png" class="img-responsive center-block" alt="My Business Card Screenshot pt2" width="20%" height="20%">

				<h4>Java Random Array Skillset</h4>
				<img src="imgs/RandomArrayDataSS7.PNG" class="img-responsive center-block" alt="Java Skillset 7 Screenshot" width="50%" height="50%">

				<h4>Java Largest of Three Numbers Skillset</h4>
				<img src="imgs/LargestOfThreeNumbersSS8.PNG" class="img-responsive center-block" alt="Java Skillset 8 Screenshot" width="50%" height="50%">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
