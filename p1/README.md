# LIS4381

## Jack Miller

### Project 1 Requirements:

*5 Parts:*

1. Screenshot of running application's first user interface
2. Screenshot of running application's second user interface
3. Java SkillSet 7 Random Array Data
4. Java SkillSet 8 Largest of Three Numbers
5. Java SkillSet 9 Array Runtime Data Validation

#### Assignment Screenshots:

First Screen            |  Second Screen
:-------------------------:|:-------------------------:
![My Business Card's First User Interface screen](imgs/Screen1.PNG)  |  ![My Business Card's Second User Interface screen](imgs/Screen2.PNG)


Random Array Data Java SkillSet            |  Largest of Three Numbers Java SkillSet |     Array Runtime Data Validation
:-------------------------:|:-------------------------:|:-------------------------:
![Random Array Data Java SkillSet](imgs/RandomArrayDataSS7.PNG)  |  ![Largest of Three Numbers Java SkillSet](imgs/LargestOfThreeNumbersSS8.PNG)   |   ![Array Runtime Data Validation](imgs/JavaSS3.PNG)
*Screenshot of Even or Odds Number Test Java SkillSet 7*

![Random Array Data Java SkillSet](imgs/RandomArrayDataSS7.PNG)

*Screenshot of Largest of Three Numbers Java SkillSet Java SkillSet 8*

![Largest of Three Numbers Java SkillSet](imgs/LargestOfThreeNumbersSS8.PNG)

*Screenshot of Array Runtime Data Validation Java SkillSet 9*

![Array Runtime Data Validation](imgs/JavaSS3.PNG)
