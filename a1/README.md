# LIS4381

## Jack H. Miller

### a1 Requirements:

*Four parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development installations
3. Questions
4. Bitbucket repo links a.) This assignment and b.) the completed tutorials above (bitbucketstationslocation)

#### README.md file should include the following items:

* Installation of AMPPS
* Download and Installation of JavaJDK
* Android Studio first app build
* Provide Screenshots of installations
* Bitbucket Repo Creation
* Complete Bitbucket tutorials
* Provide git command descriptions

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/)

> #### Git commands w/short descriptions:

1. ## git -init   (Create an empty Git repository or reinitialize an existing one)
2. ## git -status   (Show the working tree status)
3. ## git -add   (Add file contents to the index)

4. ## git -commit   (Record changes to the repository)
5. ## git -push   (Update remote refs along with associated objects)
6. ## git -pull  (Fetch from and integrate with another repository or a local branch)
7. ## git -version   (Tells the current git version)

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![C:\Users\jacmi\repos\lis4381\a1new](img/ampps.png)

*Screenshot of running java Hello*:

![C:\Users\jacmi\repos\lis4381\a1new](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![C:\Users\jacmi\repos\lis4381\a1new](img/HelloJackApp.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[C:\Users\jacmi\repos\lis4381\bitbucketstationlocations](https://bitbucket.org/jhm17b/bitbucketstationlocations/ "Bitbucket Station Locations")