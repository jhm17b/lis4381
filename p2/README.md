# LIS4381

## Jack H. Miller

### Project 2 Requirements:

*5 Parts:*

1. Screenshot of Editing Petstore Records Form
2. Screenshot of Deleting Petstore Records
3. Screenshot of Deleted Petstore Record
4. Screenshot of Failed Validation
5. Screenshots of RSS Feed
  

#### Assignment Screenshots:

Home Screen
![Home Screen](img/HomeScreen.JPG)

Edit Form           |  Delete Screen   |  Deleted Record Screen
:-------------------------:|:-------------------------:|:-------------------------:
![P2 Edit Petstore Form](img/EditScreen.JPG)  |  ![P2 Delete Screen](img/DeleteScreen.JPG)  |  ![P2 Deleted Record Screen](img/DeletedRecord.JPG)

P2 Edit Petstore Form
![P2 Edit Petstore Form](img/EditScreen.JPG)

P2 Delete Screen
![P2 Delete Screen](img/DeleteScreen.JPG)

P2 Deleted Record
![P2 Deleted Record](img/DeletedRecord.JPG)

Failed Validation
![Failed Validation](img/FailedValidation.JPG)

P2 RSS Feed
![A5 Displayed Petstore Records](img/RSSFeed.JPG)

