# LIS4381

## Jack H. Miller

### Assignment 3 Requirements:

*6 Parts:*

1. Screenshot of Petstore ERD
2. Screenshot of running Application first user interface
3. Screenshot of running Application second user interface
4. Screenshots of 10 records for each table
5. Screenshots of Java Skillsets 4-6 
6. Links to the following files
    a. a3.mwb
    b. a3.sql   

#### Assignment Screenshots:

First Screen            |  Second Screen
:-------------------------:|:-------------------------:
![Application's First User Interface screen](imgs/Screen1.PNG)  |  ![Application's Second User Interface screen](imgs/Screen2.PNG)

Petstore Records            
![Petstore ERD](imgs/petstoreERD.png)

Petstore Records            |  Customer Records         |  Pet Records
:-------------------------:|:-------------------------:|:-------------------------:
![Petstore Records](imgs/PetstoreRecords.PNG)  |  ![Customer Records](imgs/CustomerRecords.PNG)   |  ![Pet Records](imgs/petrecords.PNG)

Petstore Records
![Petstore Records](imgs/PetstoreRecords.PNG)

Customer Records
![Customer Records](imgs/CustomerRecords.PNG)

Pet Records
![Pet Records](imgs/petrecords.PNG)

Java SS4 Decision Structures Pt.1           |  Java SS4 Decision Structures Pt.2  |  Java SS4 Decision Structures Pt.3
:-------------------------:|:-------------------------:|:-------------------------:
![Java SS4 Decision Structures Pt.1 ](imgs/DecisionStructurespt1SS4.PNG)  |  ![Java SS4 Decision Structures Pt.2 ](imgs/DecisionStructurespt2SS4.PNG)  |  ![Java SS4 Decision Structures Pt.3 ](imgs/DecisionStructurespt3SS4.PNG)

Java SS4 Decision Structures pt.1
![Java SS4 Decision Structures pt.1](imgs/DecisionStructurespt1SS4.PNG)

Java SS4 Decision Structures pt.2
![Java SS4 Decision Structures pt.2](imgs/DecisionStructurespt2SS4.PNG)

Java SS4 Decision Structures pt.3
![Java SS4 Decision Structures pt.3](imgs/DecisionStructurespt3SS4.PNG)

Java SS5 Search Array
![Java SS5 Search Array](imgs/SearchArraySS5.PNG)

Java SS6 Methods
![Java SS6 Methods](imgs/MethodsSS6.PNG)


