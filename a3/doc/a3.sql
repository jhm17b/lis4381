-- MySQL Script generated by MySQL Workbench
-- Wed Oct  6 19:17:59 2021
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema jhm17b
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `jhm17b` ;

-- -----------------------------------------------------
-- Schema jhm17b
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `jhm17b` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
SHOW WARNINGS;
USE `jhm17b` ;

-- -----------------------------------------------------
-- Table `jhm17b`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jhm17b`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `jhm17b`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) UNSIGNED NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `jhm17b`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jhm17b`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `jhm17b`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(30) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT UNSIGNED NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(8,2) NOT NULL,
  `cus_total_sales` DECIMAL(8,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `jhm17b`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jhm17b`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `jhm17b`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(30) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_customer_idx` (`cus_id` ASC),
  INDEX `fk_pet_petstore1_idx` (`pst_id` ASC),
  CONSTRAINT `fk_pet_customer`
    FOREIGN KEY (`cus_id`)
    REFERENCES `jhm17b`.`customer` (`cus_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_petstore1`
    FOREIGN KEY (`pst_id`)
    REFERENCES `jhm17b`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `jhm17b`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `jhm17b`;
INSERT INTO `jhm17b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pets R Us', '300 Apple St', 'Sarasota', 'FL', 34238, 9418881212, 'PetsRUs@gmail.com', 'www.Pets-R-Us.com', 50000, NULL);
INSERT INTO `jhm17b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'PetsPetsPets', '4050 Green Way', 'Tampa', 'FL', 34288, 9509428000, 'Petspetspets@gmail.com', 'www.petspetspets.com', 90000, NULL);
INSERT INTO `jhm17b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'DogsnCats', '349 Adams St', 'Sarasota', 'FL', 34241, 9416008080, 'Dogsncats@gmail.com', 'www.dogsncats.com', 30000, NULL);
INSERT INTO `jhm17b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'DogShoppe', '2234 Vista View Dr', 'Tallahassee', 'FL', 32304, 8502507675, 'DogShoppe@gmai.com', 'www.Dogshoppe.com', 25000, NULL);
INSERT INTO `jhm17b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'CatVille', '2345 Fall Way', 'Jacksonville', 'FL', 32907, 7502209961, 'CatVille@gmail.com', 'www.Catville.com', 33500, NULL);
INSERT INTO `jhm17b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'PetLand', '3000 Gaines St', 'Tallahassee', 'FL', 32304, 8509997512, 'PetLand@gmail.com', 'www.PetLand.com', 25000, NULL);
INSERT INTO `jhm17b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Claws n Paws', '123 Main St', 'L.A', 'CA', 90195314, 8185551234, 'ClawsnPaws@gmail.com', 'www.ClawsnPaws.com', 30000, NULL);
INSERT INTO `jhm17b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Furry Friends', '1435 Chase Dr', 'Chicago', 'IL', 618959031, 8471234567, 'FurryFriends@gmail.com', 'www.FurryFriends.com', 45000, NULL);
INSERT INTO `jhm17b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Partners', '811 4th St', 'NY', 'NY', 1234567, 2125678901, 'PetPartners@gmail.com', 'www.PetPartners.com', 90000, NULL);
INSERT INTO `jhm17b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Palace', '3450 Bay St.', 'Sarasota', 'FL', 34238, 9415556767, 'PetPalace@gmail.com', 'www.PetPalace.com', 88000, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `jhm17b`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `jhm17b`;
INSERT INTO `jhm17b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Billy', 'Bob', '3000 West St', 'Sarasota', 'FL', 34241, 9418889000, 'BillyBob@gmail.com', 1000.50, 900.50, NULL);
INSERT INTO `jhm17b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Sarah', 'Sue', '3001 West St', 'Sarasota', 'FL', 34241, 9418889001, 'SarahSue@gmail.com', 950.50, 850.50, NULL);
INSERT INTO `jhm17b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Derek', 'Bob', '3000 East St', 'Sarasota', 'FL', 34238, 9417779040, 'DerekBob@gmail.com', 50.50, 10.50, NULL);
INSERT INTO `jhm17b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Steve', 'Urkle', '1200 Green Dr', 'Tallahassee', 'FL', 32304, 8501214444, 'SteveUrkle@gmail.com', 10.50, 1.50, NULL);
INSERT INTO `jhm17b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'George', 'Guy', '200 Adams Way', 'Tallahassee', 'FL', 32304, 8503337070, 'GeorgeGuy@gmail.com', 850.50, 750.10, NULL);
INSERT INTO `jhm17b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Brenda', 'Barbra', '300 Adams Way', 'Tallahassee', 'FL', 32304, 8501234567, 'BrendaBarbra@gmail.com', 700.70, 600.70, NULL);
INSERT INTO `jhm17b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Ryan', 'Fall', '1234 Vista Dr', 'Jacksonville', 'FL', 33350, 6501245555, 'RyanFall@gmail.com', 600.40, 400.60, NULL);
INSERT INTO `jhm17b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Carl', 'Brown', '2500 Vista Dr', 'Jacksonville', 'FL', 33350, 6507771345, 'CarlBrown@gmail.com', 500.40, 20.50, NULL);
INSERT INTO `jhm17b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Sally', 'Green', '2000 Falls way', 'Sarasota', 'FL', 34238, 5559991212, 'SallyGreen@gmail.com', 500.50, 30.30, NULL);
INSERT INTO `jhm17b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Kyle', 'Vyle', '300 3rd St', 'NY', 'NY', 10010, 2123334545, 'KyleVyle@gmail.com', 20.20, 10.10, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `jhm17b`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `jhm17b`;
INSERT INTO `jhm17b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 1, 'Doberman', 'm', 175, 500, 50, 'black', '2010-07-07', 'y', 'y', NULL);
INSERT INTO `jhm17b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 2, 'Chiwawa', 'm', 100, 299, 10, 'tan', '2011-08-08', 'y', 'y', NULL);
INSERT INTO `jhm17b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 3, 'Lab', 'f', 200, 600, 1, 'gold', NULL, 'y', 'y', NULL);
INSERT INTO `jhm17b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, 4, 'Lab', 'f', 200, 600, 1, 'gold', NULL, 'y', 'y', NULL);
INSERT INTO `jhm17b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, 5, 'Doodle', 'f', 300, 700, 2, 'gold', '2015-09-10', 'y', 'n', NULL);
INSERT INTO `jhm17b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, 6, 'Pitbull', 'm', 100, 300, 2, 'brown', '2015-09-10', 'y', 'n', NULL);
INSERT INTO `jhm17b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 4, 7, 'Doberman', 'm', 300, 800, 10, 'brown', NULL, 'y', 'y', NULL);
INSERT INTO `jhm17b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 4, 8, 'Doodle', 'm', 500, 900, 11, 'black', '2016-01-11', 'y', 'y', NULL);
INSERT INTO `jhm17b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 9, 'Chiwawa', 'f', 400, 600, 12, 'black', '2009-01-02', 'y', 'n', NULL);
INSERT INTO `jhm17b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 8, 10, 'Lab', 'f', 500, 900, 20, 'black', '2013-01-05', 'y', 'y', NULL);

COMMIT;

