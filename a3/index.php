<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Jack H. Miller Senior IT Major">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Assignment 3 Requirements:</strong> 6 Parts:

Screenshot of Petstore ERD
Screenshot of running Application first user interface
Screenshot of running Application second user interface
Screenshots of 10 records for each table
Screenshots of Java Skillsets 4-6
Links to the following files a. a3.mwb b. a3.sql
				</p>

				<h4>Petstore ERD</h4>
				<img src="imgs/petstoreERD.png" class="img-responsive center-block" alt="Petstore ERD" width="50%" height="50%">

				<h4>Petstore Records</h4>
				<img src="imgs/PetstoreRecords.PNG" class="img-responsive center-block" alt="Petstore Record Screenshot" width="50%" height="50%">

				<h4>Pet Records</h4>
				<img src="imgs/petrecords.PNG" class="img-responsive center-block" alt="Pet Record Screenshot" width="50%" height="50%">

				<h4>Customer Records</h4>
				<img src="imgs/CustomerRecords.PNG" class="img-responsive center-block" alt="Customer Record Screenshot" width="50%" height="50%">

				<h4>Concert Ticket Calculator App</h4>
				<img src="imgs/screen1.png" class="img-responsive center-block" alt="Concert Ticket App Screenshot" width="20%" height="20%">

				<h4>Concert Ticket Calculator App Pt.2</h4>
				<img src="imgs/screen2.png" class="img-responsive center-block" alt="Concert Ticket App Screenshot pt2" width="20%" height="20%">

				<h4>Java Decision Structure Skillset</h4>
				<img src="imgs/DecisionStructurespt1SS4.PNG" class="img-responsive center-block" alt="Java Skillset 4 Screenshot" width="50%" height="50%">

				<h4>Java Decision Structure Skillset Pt2</h4>
				<img src="imgs/DecisionStructurespt2SS4.PNG" class="img-responsive center-block" alt="Java Skillset 4 Screenshot" width="50%" height="50%">

				<h4>Java Decision Structure Skillset Pt3</h4>
				<img src="imgs/DecisionStructurespt3SS4.PNG" class="img-responsive center-block" alt="Java Skillset 4 Screenshot" width="50%" height="50%">

				<h4>Java Search Array Skillset</h4>
				<img src="imgs/SearchArraySS5.PNG" class="img-responsive center-block" alt="Java Skillset 5 Screenshot" width="50%" height="50%">

				<h4>Java Methods Skillset</h4>
				<img src="imgs/MethodsSS6.PNG" class="img-responsive center-block" alt="Java Skillset 6 Screenshot" width="50%" height="50%">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
