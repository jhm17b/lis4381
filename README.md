# LIS4381

## Jack H. Miller

### LIS4381 Requirements:
*Course Work links:*

### Assignments:
1. [A1 README.md](https://bitbucket.org/jhm17b/lis4381/src/master/a1/README.md)

    * Installation of AMPPS
    * Download and Installation of JavaJDK
    * Android Studio first app build
    * Provide Screenshots of installations
    * Bitbucket Repo Creation
    * Complete Bitbucket tutorials
    * Provide git command descriptions

#### a2 README.md file includes the following items:
2. [A2 README.md](https://bitbucket.org/jhm17b/lis4381/src/master/a2/README.md)
    * Creation of Healthy Recipes application
    * Even or Odds test Java SkillSet 1
    * Largest Number test Java SkillSet 2
    * Arrays and Loops Java SkillSet 3

#### a3 README.md file includes the following items:
3. [A3 README.md](https://bitbucket.org/jhm17b/lis4381/src/master/a3/README.md)
    * Screenshot of Petstore ERD
    * Screenshot of running Application first user interface
    * Screenshot of running Application second user interface
    * Screenshots of 10 records for each table
    * Screenshots of Java Skillsets 4-6 
    * Links to the following files
        * a. a3.mwb
        * b. a3.sql

#### a4 README.md file includes the following items:
4. [A4 README.md](https://bitbucket.org/jhm17b/lis4381/src/master/a4/README.md)

    * Screenshot of Petstore clientside validation working
    * Screenshot of Petstore clientside validation not working
    * Screenshot of running My Online Portfolio homepage
    * Screenshots of Java Skillsets 10-12

#### a5 README.md file includes the following items:
5. [A5 README.md](https://bitbucket.org/jhm17b/lis4381/src/master/a5/README.md)

    * Screenshot of Petstore Serverside validation not working
    * Screenshot of running Petstore Database Records
    * Screenshots of Java Skillsets 13-15

### Projects:

#### p1 README.md file includes the following items:
1. [p1 README.md](https://bitbucket.org/jhm17b/lis4381/src/master/p1/README.md)
    * Screenshot of running My Business Card's first user interface
    * Screenshot of running My Business Card's second user interface
    * Java SkillSet 7 Random Array Data
    * Java SkillSet 8 Largest of Three Numbers
    * Java SkillSet 9 Array Runtime Data Validation
#### p2 README.md file includes the following items:
1. [p2 README.md](https://bitbucket.org/jhm17b/lis4381/src/master/p2/README.md)
    * Screenshot of Editing Petstore Records Form
    * Screenshot of Deleting Petstore Records
    * Screenshot of Deleted Petstore Record
    * Screenshot of Failed Validation
    * Screenshots of RSS Feed
