import java.util.Scanner;
class main
{
  public static void main(String args[])
  {
      //call static void method
      methods.getRequirements();
      
      //Java style String[] myArray
      //C++ style String myArray[]
      //call createArray() method in method class
      //returns initialized array, array size determined by user
      int[] userArray = methods.createArray(); //java style array

      //call generatePseudoRandomNumber() method, passing returned array above
      //prints pseudo randomly generated numbers, determined by number user inputs
      methods.generatePseudoRandomNumbers(userArray); //pass array
      
  }
}