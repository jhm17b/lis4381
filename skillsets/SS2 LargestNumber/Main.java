import java.util.Scanner;
class Main{
 public static void main(String args[]) 
    {
        methods.getRequirements();

        System.out.print("***Call static (no object) void (non-value returning) method: ***");
        //call static void method(i.e no object, non-value returning)
        methods.largestNumber();

        //or
        System.out.print("\n***Call static (no object) value-returning method and void method:***");
        //declare variables and create scanner object
        int myNum1=0, myNum2=0;

        System.out.print("Enter first integer: ");
        myNum1 = methods.getNum();

        System.out.print("Enter second integer: ");
        myNum2 = methods.getNum();

        //call void method passing user input
        //methods.evaluateNumber();
    }
}

