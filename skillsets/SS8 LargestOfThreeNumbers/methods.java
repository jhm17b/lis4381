import java.util.Scanner;

public class methods
{


    //nonvalue-returning method (w/o object - static)
    public static void getRequirements()
    {
        //Display operational msgs
        System.out.println("Developer: Jack H. Miller");
        System.out.println("Program evaluates largest of three numbers");
        System.out.println("Note: Program checks for integers and non-numeric values.");
    
        System.out.println(); //Prints blank line
    }

    public static void validateUserInput()
    {
        Scanner sc = new Scanner(System.in);
        int num1 = 0, num2 = 0, num3 = 0;

        //prompt user for three integers
        System.out.print("Please enter first number: ");
        while (!sc.hasNextInt())
            {
                System.out.println("Not a valid integer!\n");
                sc.next(); //Important! If omitted will go into infinit loop on invalid input!
                System.out.print("Please try again. Enter first number: ");
            }
        num1 = sc.nextInt();

        System.out.print("Please enter second number: ");
        while (!sc.hasNextInt())
            {
                System.out.println("Not a valid integer!\n");
                sc.next(); //Important! If omitted will go into infinit loop on invalid input!
                System.out.print("Please try again. Enter second number: ");
            }
        num2 = sc.nextInt();

        System.out.print("Please enter third number: ");
        while (!sc.hasNextInt())
            {
                System.out.println("Not a valid integer!\n");
                sc.next(); //Important! If omitted will go into infinit loop on invalid input!
                System.out.print("Please try again. Enter third number: ");
            }
        num3 = sc.nextInt();

        System.out.println(); //blank line
        getLargestNumber(num1, num2, num3);
    }

    public static void getLargestNumber(int num1, int num2, int num3)
    {
        System.out.println("Numbers entered: " + num1 + ", " + num2 + ", " + num3);

        if (num1 > num2 && num1 > num3)
          System.out.println(num1 + " is largest.");
        else if (num2 > num1 && num2 > num3)
          System.out.println(num2 + " is largest.");
        else if (num3 > num1 && num3 > num2)
          System.out.println(num3 + " is largest.");  
    }



    public static void getUserInput()
    {
        //initialize variables, create scanner object, capture user input
        String firstName="";
        int userAge=0;
        String myStr="";
        Scanner sc = new Scanner(System.in);

        //input
        System.out.print("Enter first name: ");
        firstName = sc.next();

        System.out.print("Enter age: ");
        userAge = sc.nextInt();

        System.out.println();

        //call void method
        System.out.print("void method call: ");
        myVoidMethod(firstName, userAge);

        //call value returning method
        System.out.print("value returning method call: ");
        myStr = myValueReturningMethod(firstName, userAge);
        System.out.println(myStr);
    }
    //Note:both methods use *same* named parameters.. which are *local* variables

    public static void myVoidMethod(String first, int age)
    {
        System.out.println(first + " is " + age);
        return;
        
    }
    public static String myValueReturningMethod(String first, int age)
    {
        return first + " is " + age;
    }

    //nonvalue returning (void) method (static requires no object)
    public static void searchArray()
    {
        int nums[] = {3, 2, 4, 99, -1, -5, 3, 7};
        Scanner sc = new Scanner(System.in);
        int search;

        System.out.print("Array length: " + nums.length);

        System.out.print("\nEnter search value: ");
        search = sc.nextInt();

        System.out.println(); //blank line
        for (int i = 0; i < nums.length;i++)
        {
            if(nums[i] == search)
            {
                System.out.println(search + " found at index " + i);
            }
            else
            {
                System.out.println(search + " *not* found at index " + i);
            }
        }
    }

    public static void getUserPhoneType()
    {
        String myStr="";
        char myChar=' ';
        Scanner sc = new Scanner(System.in);

        /*
        Note: currently, there is no API method to get a character from the scanner.
        Solution: get String using scanner.next() and invoke String.charAt(0) method on return String.
        */
        System.out.println("Phone types: W or w (work), C or c (cell), H or h (home), N or n (none).");
        System.out.println("Enter phone type: ");
        myStr = sc.next().toLowerCase();
        myChar = myStr.charAt(0);

        System.out.println("\nif...else:");

        if(myChar == 'w')
         System.out.println("Phone type: work");
        else if (myChar == 'c')
         System.out.println("Phone type: cell");
        else if (myChar == 'h')
         System.out.println("Phone type: home");
        else if (myChar == 'n')
         System.out.println("Phone type: none"); 
        else
        System.out.println("Incorrect character entry."); 

        System.out.println(""); //Blank line
        System.out.println("switch:");
        switch(myChar)
        {
            case 'w':
              System.out.println("Phone type: work");
              break;
            case 'c':
              System.out.println("Phone type: cell");
              break;
            case 'h':
              System.out.println("Phone type: home");
              break;
            case 'n':
              System.out.println("Phone type: none");
              break;
            default:
              System.out.println("Incorrect character entry.");
              break;  

        }



    }
    public static void evaluateNumber()
    {
        //initialize variables, create Scanner object, capture user input
        int x=0;
        System.out.print("Enter integer: ");
        Scanner sc = new Scanner(System.in);
        x = sc.nextInt();

        if ( x % 2 == 0)
        {
            System.out.println(x + " is an even number.");
        }
        else
        {
            System.out.println(x + " is an odd number.");
        }
    }


    public static void largestNumber()
    {
        //declares variables and creates scanner object
        int num1, num2;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter first integer: ");
        num1 = sc.nextInt();

            System.out.println("Enter secon integer: ");
            num2 = sc.nextInt();

            System.out.println(); //blank line
            if(num1 > num2)
            System.out.println(num1 + " is larger than " + num2);
             else if(num2 > num1)
            System.out.println(num2 + " is larger than " + num1);
             else
            System.out.println("Integers are equal.");
    }

    public static int getNum()
    {
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }
    public static void arrayLoop()
    {
        //populate array (zero based)
    /*
    animals[0] = "dog";
    animals[1] = "cat";
    animals[2] = "bird";
    animals[3] = "fish";
    animals[4] = "insect";
    */

    //or, populate upon creation...
    //Java style String[] animals

    String animals[] = {"dog", "cat", "bird", "fish", "insect"};

    System.out.println("for loop:");
    for(int i=0; i < animals.length;i++)
    {
        System.out.println(animals[i]);
    }

    //Note: enchanced for loop does not require an iterator (e.g, x or or j).
    //Loop iterates through each element of array/collection.
    System.out.println("\nEnchanced for loop:");
    for(String test : animals)
    {
        System.out.println(test);
    }

    System.out.println("\nwhile loop:");
    int i=0;
    while (i < animals.length)
    {
        System.out.println(animals[i]);
        i++;
    }

    i=0; //reassign 0 to test variable
    System.out.println("\ndo...while loop:");
    do
      {
        System.out.println(animals[i]);
        i++;
      }
    while (i < animals.length);  
    }
    
    
}