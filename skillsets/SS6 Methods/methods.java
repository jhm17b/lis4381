import java.util.Scanner;

public class methods
{


    //nonvalue-returning method (w/o object - static)
    public static void getRequirements()
    {
        //Display operational msgs
        System.out.println("Developer: Jack H. Miller");
        System.out.println("Program prompts user for first name and age, then prints results.");
        System.out.println("Create four methods from the following requirements:");
        System.out.println("1) getRequirements(): Void method displays program requirements.");

        System.out.println("2)getUserInput(): Void method prompts for user input,  \n\t then calls two methods : myVoidMethod() and myValueReturningMethod().");

        System.out.println("3) myVoidMethod()\n" + "\ta. Accepts two arguments: String and int. \n\tb. Prints user's first name and age.");
        System.out.println("4) myValueReturningMethod(): \n" + "\ta. Accepts two arguments: String and in \n" + "\tb. Returns String containing name and age.");
        
        System.out.println(); //Prints blank line
    }
    public static void getUserInput()
    {
        //initialize variables, create scanner object, capture user input
        String firstName="";
        int userAge=0;
        String myStr="";
        Scanner sc = new Scanner(System.in);

        //input
        System.out.print("Enter first name: ");
        firstName = sc.next();

        System.out.print("Enter age: ");
        userAge = sc.nextInt();

        System.out.println();

        //call void method
        System.out.print("void method call: ");
        myVoidMethod(firstName, userAge);

        //call value returning method
        System.out.print("value returning method call: ");
        myStr = myValueReturningMethod(firstName, userAge);
        System.out.println(myStr);
    }
    //Note:both methods use *same* named parameters.. which are *local* variables

    public static void myVoidMethod(String first, int age)
    {
        System.out.println(first + " is " + age);
        return;
        
    }
    public static String myValueReturningMethod(String first, int age)
    {
        return first + " is " + age;
    }

    //nonvalue returning (void) method (static requires no object)
    public static void searchArray()
    {
        int nums[] = {3, 2, 4, 99, -1, -5, 3, 7};
        Scanner sc = new Scanner(System.in);
        int search;

        System.out.print("Array length: " + nums.length);

        System.out.print("\nEnter search value: ");
        search = sc.nextInt();

        System.out.println(); //blank line
        for (int i = 0; i < nums.length;i++)
        {
            if(nums[i] == search)
            {
                System.out.println(search + " found at index " + i);
            }
            else
            {
                System.out.println(search + " *not* found at index " + i);
            }
        }
    }

    public static void getUserPhoneType()
    {
        String myStr="";
        char myChar=' ';
        Scanner sc = new Scanner(System.in);

        /*
        Note: currently, there is no API method to get a character from the scanner.
        Solution: get String using scanner.next() and invoke String.charAt(0) method on return String.
        */
        System.out.println("Phone types: W or w (work), C or c (cell), H or h (home), N or n (none).");
        System.out.println("Enter phone type: ");
        myStr = sc.next().toLowerCase();
        myChar = myStr.charAt(0);

        System.out.println("\nif...else:");

        if(myChar == 'w')
         System.out.println("Phone type: work");
        else if (myChar == 'c')
         System.out.println("Phone type: cell");
        else if (myChar == 'h')
         System.out.println("Phone type: home");
        else if (myChar == 'n')
         System.out.println("Phone type: none"); 
        else
        System.out.println("Incorrect character entry."); 

        System.out.println(""); //Blank line
        System.out.println("switch:");
        switch(myChar)
        {
            case 'w':
              System.out.println("Phone type: work");
              break;
            case 'c':
              System.out.println("Phone type: cell");
              break;
            case 'h':
              System.out.println("Phone type: home");
              break;
            case 'n':
              System.out.println("Phone type: none");
              break;
            default:
              System.out.println("Incorrect character entry.");
              break;  

        }



    }
    public static void evaluateNumber()
    {
        //initialize variables, create Scanner object, capture user input
        int x=0;
        System.out.print("Enter integer: ");
        Scanner sc = new Scanner(System.in);
        x = sc.nextInt();

        if ( x % 2 == 0)
        {
            System.out.println(x + " is an even number.");
        }
        else
        {
            System.out.println(x + " is an odd number.");
        }
    }


    public static void largestNumber()
    {
        //declares variables and creates scanner object
        int num1, num2;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter first integer: ");
        num1 = sc.nextInt();

            System.out.println("Enter secon integer: ");
            num2 = sc.nextInt();

            System.out.println(); //blank line
            if(num1 > num2)
            System.out.println(num1 + " is larger than " + num2);
             else if(num2 > num1)
            System.out.println(num2 + " is larger than " + num1);
             else
            System.out.println("Integers are equal.");
    }

    public static int getNum()
    {
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }
    public static void arrayLoop()
    {
        //populate array (zero based)
    /*
    animals[0] = "dog";
    animals[1] = "cat";
    animals[2] = "bird";
    animals[3] = "fish";
    animals[4] = "insect";
    */

    //or, populate upon creation...
    //Java style String[] animals

    String animals[] = {"dog", "cat", "bird", "fish", "insect"};

    System.out.println("for loop:");
    for(int i=0; i < animals.length;i++)
    {
        System.out.println(animals[i]);
    }

    //Note: enchanced for loop does not require an iterator (e.g, x or or j).
    //Loop iterates through each element of array/collection.
    System.out.println("\nEnchanced for loop:");
    for(String test : animals)
    {
        System.out.println(test);
    }

    System.out.println("\nwhile loop:");
    int i=0;
    while (i < animals.length)
    {
        System.out.println(animals[i]);
        i++;
    }

    i=0; //reassign 0 to test variable
    System.out.println("\ndo...while loop:");
    do
      {
        System.out.println(animals[i]);
        i++;
      }
    while (i < animals.length);  
    }
    
    
}