import java.util.Scanner;

public class methods
{


    //nonvalue-returning method (w/o object - static)
    public static void getRequirements()
    {
        //Display operational msgs
        System.out.println("Developer:Jack H. Miller");
        System.out.println("Program loops through array of strings");
        System.out.println("Use following loop structures: for, enhanced for, while, do...while.");
        System.out.println("\nNote: Pretest loops: for, enhanced for, while, Posttest loop: do...while.");
        System.out.println(); //Prints blank line
    }
    public static void evaluateNumber()
    {
        //initialize variables, create Scanner object, capture user input
        int x=0;
        System.out.print("Enter integer: ");
        Scanner sc = new Scanner(System.in);
        x = sc.nextInt();

        if ( x % 2 == 0)
        {
            System.out.println(x + " is an even number.");
        }
        else
        {
            System.out.println(x + " is an odd number.");
        }
    }


    public static void largestNumber()
    {
        //declares variables and creates scanner object
        int num1, num2;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter first integer: ");
        num1 = sc.nextInt();

            System.out.println("Enter secon integer: ");
            num2 = sc.nextInt();

            System.out.println(); //blank line
            if(num1 > num2)
            System.out.println(num1 + " is larger than " + num2);
             else if(num2 > num1)
            System.out.println(num2 + " is larger than " + num1);
             else
            System.out.println("Integers are equal.");
    }

    public static int getNum()
    {
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }
    public static void arrayLoop()
    {
        //populate array (zero based)
    /*
    animals[0] = "dog";
    animals[1] = "cat";
    animals[2] = "bird";
    animals[3] = "fish";
    animals[4] = "insect";
    */

    //or, populate upon creation...
    //Java style String[] animals

    String animals[] = {"dog", "cat", "bird", "fish", "insect"};

    System.out.println("for loop:");
    for(int i=0; i < animals.length;i++)
    {
        System.out.println(animals[i]);
    }

    //Note: enchanced for loop does not require an iterator (e.g, x or or j).
    //Loop iterates through each element of array/collection.
    System.out.println("\nEnchanced for loop:");
    for(String test : animals)
    {
        System.out.println(test);
    }

    System.out.println("\nwhile loop:");
    int i=0;
    while (i < animals.length)
    {
        System.out.println(animals[i]);
        i++;
    }

    i=0; //reassign 0 to test variable
    System.out.println("\ndo...while loop:");
    do
      {
        System.out.println(animals[i]);
        i++;
      }
    while (i < animals.length);  
    }
    
    
}