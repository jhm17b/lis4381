//Array vs ArrayList
/*
1.)Resizable:
Array: static (fixed length) data structure- cant change after creation
ArrayList:dynamic

2.)Data Types:
Array: contain both primitive data types (int, float, double) as well as objects
ArrayList: can NOT contain primitive data types, only objects

3.) Access and Modify data:
Array: members accessed using []
ArrayList:set of methods to access elements and modify them

4.) Multidimensional
Array: can be
ArrayList: only single-dimensiona
*/
import java.util.Scanner;
class main
{
  public static void main(String args[])
  {
      //call static void method
      methods.getRequirements();
      methods.arrayLoop();
  }
}
    
