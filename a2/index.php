<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Jack H. Miller Senior IT Major">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Assignment 2 Requirements:</strong> 5 Parts:

Screenshot of running application's first user interface
Screenshot of running application's second user interface
Java SkillSet 1 Evens or Odds Number Test Running
Java SkillSet 2 Largest Number Test running
Java SkillSet 3 Arrays and Loops running
				</p>

				<h4>Healthy Recipes Application</h4>
				<img src="img/screen1.png" class="img-responsive center-block" alt="Healthy Recipes Screenshot" width="20%" height="20%">

				<h4>Healthy Recipes Application Pt.2</h4>
				<img src="img/screen2.png" class="img-responsive center-block" alt="Healthy Recipes Screenshot" width="20%" height="20%">

				<h4>Java Evens or Odds Skillset</h4>
				<img src="img/JavaSS1.png" class="img-responsive center-block" alt="Java Skillset 1 Screenshot" width="50%" height="50%">

				<h4>Java Largest Number Skillset</h4>
				<img src="img/JavaSS2.png" class="img-responsive center-block" alt="Java Skillset 2 Screenshot" width="50%" height="50%">

				<h4>Java Arrays and Loops Skillset</h4>
				<img src="img/JavaSS3.png" class="img-responsive center-block" alt="Java Skillset 3 Screenshot" width="50%" height="50%">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
