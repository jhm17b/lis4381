> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Jack Miller

### Assignment 2 Requirements:

*5 Parts:*

1. Screenshot of running application's first user interface
2. Screenshot of running application's second user interface
3. Java SkillSet 1 Evens or Odds Number Test Running
4. Java SkillSet 2 Largest Number Test running
5. Java SkillSet 3 Arrays and Loops running

#### README.md file should include the following items:



#### Assignment Screenshots:

First Screen            |  Second Screen
:-------------------------:|:-------------------------:
![Application's First User Interface screen](img/Screen1.png)  |  ![Application's Second User Interface screen](img/Screen2.png)


Evens or Odds Java SkillSet            |  Largest Number Java SkillSet |     Arrays and Loops Java SkillSet
:-------------------------:|:-------------------------:|:-------------------------:
![Application's Second User Interface screen](img/JavaSS1.png)  |  ![Application's Second User Interface screen](img/JavaSS2.png)   |   ![Application's Second User Interface screen](img/JavaSS3.png)
*Screenshot of Even or Odds Number Test Java SkillSet 1*

![Application's Second User Interface screen](img/JavaSS1.png)

*Screenshot of Largest Number Test Java SkillSet 2*

![Application's Second User Interface screen](img/JavaSS2.png)

*Screenshot of Arrays and Loops Java SkillSet 3*

![Application's Second User Interface screen](img/JavaSS3.png)
