# LIS4381

## Jack H. Miller

### Assignment 5 Requirements:

*3 Parts:*

1. Screenshot of Petstore Serverside validation not working
2. Screenshot of running Petstore Database Records
3. Screenshots of Java Skillsets 13-15
  

#### Assignment Screenshots:

First Screen            |  Second Screen
:-------------------------:|:-------------------------:
![A5 Displayed Petstore Records](img/Screen1.JPG)  |  ![A5 Non-showing error](img/Screen2.JPG)

A5 Displayed Petstore Records
![A5 Displayed Petstore Records](img/Screen1.JPG)

Incorrect Validation (Not working only shows an array of the submitted data, it's most likely something tiny but I could not figure how to get the error to display I have been decently sick and simply at my wits end, client-side validation was commented out and went through the videos in whole twice and scrubbed through code as well while dealing with Crohn's flaire up. I accept the points penelty I cannot figure this out for the life of me.)
![Application's Second User Interface screen](img/Screen2.JPG)

A5 Text Form Placeholders
![A5 Displayed Petstore Records](img/Screen3.JPG)


Java SS13 Sphere Volume Calculator
![Java SS13 Sphere Volume Calculator](img/JavaSS13.JPG)

SS14 Simple Calculator
![SS14 Simple Calculator](img/SS14.JPG)

SS15 Write/Read File
![SS15 Write/Read File](img/SS15.JPG)
